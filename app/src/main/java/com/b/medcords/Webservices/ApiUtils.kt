package com.b.medcords.Webservices

object ApiUtils {
    const val BASE_URL = "https://qa-doctor.medcords.com/"

    val aPIService: WebService
        get() = RetrofitClient.getClient(BASE_URL)!!.create(WebService::class.java)
}