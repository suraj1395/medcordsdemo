package com.b.medcords.Dashboard

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.fragment.app.FragmentActivity
import androidx.viewpager.widget.PagerAdapter
import com.b.medcords.R
import java.util.ArrayList


 class DashboardAdapter (private val context: FragmentActivity?, private val images: ArrayList<Int>) :
    PagerAdapter() {
     override fun getCount(): Int {
         return images.size
     }


     private val inflater: LayoutInflater

//    val getCount: Int
//        get() = images.size

    init {
        inflater = LayoutInflater.from(context)
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }

    override fun instantiateItem(view: ViewGroup, position: Int): Any {
        val myImageLayout = inflater.inflate(R.layout.dashboard_layout, view, false)
        val myImage = myImageLayout
            .findViewById(R.id.image) as ImageView
        myImage.setImageResource(images[position])
        view.addView(myImageLayout, 0)
        return myImageLayout
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }
}
