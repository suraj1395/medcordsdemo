package com.b.medcords.Dashboard


import android.content.ContentValues.TAG
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.cardview.widget.CardView
import androidx.viewpager.widget.ViewPager
import com.b.medcords.MainActivity

import com.b.medcords.R
import com.b.medcords.Webservices.ApiUtils
import com.b.medcords.Webservices.UserInfo
import com.b.medcords.Webservices.WebService
import com.google.android.material.tabs.TabLayout
import com.google.gson.annotations.SerializedName
import kotlinx.android.synthetic.main.fragment_dashborad.*
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

/**
 * A simple [Fragment] subclass.
 */
class DashboradFragment : Fragment() {

    private var cvMenuBtn1: CardView? = null
    private var cvButton2: CardView? = null
    private var cvButton3: CardView? = null
    private var cvButton4: CardView? = null
    private var mPager: ViewPager? = null

    private var currentPage = 0
    private val img = arrayOf(R.drawable.doc, R.drawable.doc2,R.drawable.doc3)
    private val mBanner = ArrayList<Int>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle? ): View? { return inflater.inflate(R.layout.fragment_dashborad, container, false) }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        initControl()
        imgSlider()
    }


    private fun initControl() {
        cvMenuBtn1 = view!!.findViewById(R.id.cvMenuBtn1)
        cvButton2 = view!!.findViewById(R.id.cvButton2)
        cvButton3 = view!!.findViewById(R.id.cvButton3)
        cvButton4 = view!!.findViewById(R.id.cvButton4)




        cvMenuBtn1!!.setOnClickListener(){
            var userInfo = UserInfo("Button 1")
            assignButton(userInfo)
        }

        cvButton2!!.setOnClickListener(){

            var userInfo1 = UserInfo("Button 2")
            assignButton(userInfo1)

           // Toast.makeText(activity,"Button 2",Toast.LENGTH_SHORT).show()
        }
        cvButton3!!.setOnClickListener(){

            var userInfo2 = UserInfo("Button 3")
            assignButton(userInfo2)

          //  Toast.makeText(activity,"Button 3",Toast.LENGTH_SHORT).show()
        }
        cvButton4!!.setOnClickListener(){

            var userInfo3 = UserInfo("Button 4")
            assignButton(userInfo3)

           // Toast.makeText(activity,"Button 4",Toast.LENGTH_SHORT).show()
        }

    }


    private fun imgSlider() {

        for (i in img.indices)
            mBanner.add(img[i])

        mPager = view!!.findViewById(R.id.viewPager)

        mPager!!.setAdapter(activity?.let { DashboardAdapter(it, mBanner) })
        val tabLayout = view!!.findViewById<TabLayout>(R.id.tab_layout)
        tabLayout.setupWithViewPager(mPager, true)
        val handler = Handler()
        val Update = Runnable {
            if (currentPage == img.size) {
                currentPage = 0
            }
            mPager!!.setCurrentItem(currentPage++, true)
        }
        val swipeTimer = Timer()
        swipeTimer.schedule(object : TimerTask() {
            override fun run() {
                handler.post(Update)
            }
        }, 10000, 9000)
    }


    private fun assignButton(Name: UserInfo) {

            val mAPIService: WebService = ApiUtils.aPIService
            val call= mAPIService.assignButton(Name)
            call?.enqueue(object : Callback<String?> {
                override fun onFailure(call: Call<String?>, t: Throwable) {
                }
                override fun onResponse(call: Call<String?>, response: Response<String?>) {
                    var abc=response.body();
                    val jObj = JSONObject(abc)
                    val mMsg=jObj.getJSONObject("data").getString("rstr");
                    Toast.makeText(activity, mMsg, Toast.LENGTH_SHORT).show()
                }

//                override fun onResponse(callback: Call<ResponseBody?>?, response: Response<ResponseBody?>?) {
//                    val res = response!!.body()
//                  //  closeProgressBar()
//                    Log.d(TAG, "onResponse:assign  $res")
//                    if (res != null) {
////                        try {
////                            val jObj = JSONObject(res.string())
////                            //   val json: JSONObject = jObj.getJSONObject("data").getJSONObject("result"){
////
////                            if (jObj.getString("status").equals(0)) {
////
////                                val json: JSONObject = jObj.getJSONObject("data")
////
////                                var Bname1 =  json.getString("rstr")
////
////                                Toast.makeText(activity, "  Successfully  ", Toast.LENGTH_SHORT).show()
////                                //setResult(.RESULT_OK, Intent())
////                                activity!!.finish()
////                            } else {
////                                Toast.makeText(activity, "Went something wrong.", Toast.LENGTH_SHORT).show()
////                            }
////                        } catch (e: Exception) {
////                            e.printStackTrace()
////                        }
//                    } else {
//                        Toast.makeText(activity, "Please try again.", Toast.LENGTH_SHORT).show()
//                    }
//                }
//
//                override fun onFailure(call: Call<ResponseBody?>?, t: Throwable?) {
//                    Log.d(TAG, "onFailure: " + "fail")
//                 //   closeProgressBar()
//                }
            })
        }
}


