package com.b.medcords

import android.app.Dialog
import android.os.Bundle
import android.view.MenuItem
import android.view.Window
import android.widget.Button
import android.widget.ImageView
import android.widget.RelativeLayout
import androidx.annotation.NonNull
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.b.medcords.Dashboard.DashboradFragment
import com.b.medcords.Dashboard.HelpFragment
import com.b.medcords.Dashboard.ProfileFragment
import com.google.android.material.bottomnavigation.BottomNavigationView

class MainActivity : AppCompatActivity() {


    private var main_navigation: BottomNavigationView? = null
    internal lateinit var manager: FragmentManager
    internal lateinit var transaction: FragmentTransaction
    val MY_PERMISSIONS_REQUEST_CODE = 7
    private var mActivity: MainActivity? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mActivity = this@MainActivity

        setHomePage(0)

        displayView(0)
        main_navigation = findViewById(R.id.main_navigation) as BottomNavigationView

        main_navigation!!.setOnNavigationItemSelectedListener(object :
            BottomNavigationView.OnNavigationItemSelectedListener {
            override fun onNavigationItemSelected(@NonNull item: MenuItem): Boolean {
                val id = item.itemId
                when (id) {
                    R.id.menu_dashboard -> displayView(0)

                    R.id.menu_profile -> displayView(1)

                    R.id.menu_help -> displayView(2)

                }

                return true
            }
        })


    }

    private fun setHomePage(i: Int) {

        val bottomNavigationView = findViewById<BottomNavigationView>(R.id.main_navigation)
        when (i) {
            0 -> bottomNavigationView.setSelectedItemId(R.id.menu_dashboard)
            1 -> bottomNavigationView.setSelectedItemId(R.id.menu_profile)
            2 -> bottomNavigationView.setSelectedItemId(R.id.menu_help)

        }
    }

    private fun displayView(i: Int) {

        var fragment: Fragment? = null
        var title = "DASHBOARD "
        when (i) {
            0 -> {
                fragment = DashboradFragment()
                title = "DASHBOARD"
            }

            1 -> {
                fragment = ProfileFragment()
            }

            2->{
                fragment = HelpFragment()
            }


        }

        manager = supportFragmentManager
        transaction = manager.beginTransaction()
        transaction.setCustomAnimations(
            android.R.anim.fade_in,
            android.R.anim.fade_out,
            android.R.anim.fade_in,
            android.R.anim.fade_out
        )
        if (fragment != null) {
            if (supportFragmentManager.backStackEntryCount > 0) {
                if (getCurrentFragment() !== fragment)
                    transaction.replace(R.id.frag_frame, fragment)
                        .addToBackStack(null)
                        .commit()
            } else {
                transaction.replace(R.id.frag_frame, fragment)
                    .addToBackStack(null)
                    .commit()
            }
//            if (supportActionBar != null) {
//                mTitle!!.setText(title)
//                // getSupportActionBar().setTitle(title);
//            }
        }
    }

    private fun getCurrentFragment(): Fragment? {
        val fragmentManager = supportFragmentManager
        val fragmentTag =
            fragmentManager.getBackStackEntryAt(fragmentManager.backStackEntryCount -1 ).name
        return supportFragmentManager.findFragmentByTag(fragmentTag)
    }


    override fun onBackPressed() {
//        if (supportFragmentManager.backStackEntryCount > 1) {
//            super.onBackPressed()
//        } else {
//            finish()
//        }

        val bottomNavigationView =findViewById<BottomNavigationView>(R.id.main_navigation)
        if (bottomNavigationView.getSelectedItemId() == R.id.menu_dashboard)
        {
            showExitDialobox();
        } else {
            bottomNavigationView.setSelectedItemId(R.id.menu_profile)
            bottomNavigationView.setSelectedItemId(R.id.menu_help)
            bottomNavigationView.setSelectedItemId(R.id.menu_dashboard)


        }
    }

    fun showExitDialobox() {
        val dialog = Dialog(this@MainActivity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.custom_dialog_layout)
        val dialogButtonYes = dialog.findViewById(R.id.btnYes) as Button
        val dialogButtonNo = dialog.findViewById(R.id.btnNo) as Button
        val image = dialog.findViewById(R.id.imgCancel) as ImageView
        val rlDialog = dialog.findViewById(R.id.rlDialog) as RelativeLayout
        rlDialog.setOnClickListener { dialog.dismiss() }

        dialogButtonYes.setOnClickListener {
            dialog.dismiss()
            finish()
        }

        dialogButtonNo.setOnClickListener { dialog.dismiss() }
        image.setOnClickListener { dialog.dismiss() }
        dialog.show()
    }

}